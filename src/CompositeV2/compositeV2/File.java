package CompositeV2.compositeV2;

public class File extends AbstractElement{

    public File(String nameElement) {
        super(nameElement);
    }

    @Override
    public void add(Element element) {
        throw new UnsupportedOperationException(); //Нельзя же так!????
    }

    @Override
    public void remove(Element element) {
        throw new UnsupportedOperationException(); //Нельзя же так!????
    }

    @Override
    public void showElements() {
        System.out.println(getNameElement());
    }
}
