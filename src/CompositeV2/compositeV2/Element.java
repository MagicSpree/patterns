package CompositeV2.compositeV2;

public interface Element {
    void add(Element element);
    void remove(Element element);
    void showElements();
}
