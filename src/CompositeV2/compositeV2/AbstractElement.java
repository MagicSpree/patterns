package CompositeV2.compositeV2;


public abstract class AbstractElement implements Element {
    private final String nameElement;

    public AbstractElement(String nameElement) {
        this.nameElement = nameElement;
    }

    public String getNameElement() {
        return nameElement;
    }
}
