package CompositeV2.compositeV2;

import CompositeV2.compositeV2.AbstractElement;
import CompositeV2.compositeV2.Element;

import java.util.ArrayList;
import java.util.List;

public class Folder extends AbstractElement {

    List<Element> children = new ArrayList<>();

    public Folder(String nameElement) {
        super(nameElement);
    }

    @Override
    public void add(Element element) {
        children.add(element);
    }

    @Override
    public void remove(Element element) {
        children.remove(element);
    }

    @Override
    public void showElements() {
        System.out.println("Папка: " + getNameElement());
        System.out.println("Содержимое папки " + getNameElement() + ":");

        for (Element element : children) {
            element.showElements();
        }
    }
}
