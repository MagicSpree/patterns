package CompositeV2;

import CompositeV2.compositeV2.Element;
import CompositeV2.compositeV2.Folder;
import CompositeV2.compositeV2.File;

public class Main {

    public static void main(String[] args) {
        Element disk = new Folder("Диск С");
        Element studyFolder1Curs = new Folder("Обучение 1 курс");

        Element firstLabDocx = new File("1lab.docx");
        Element practiceReport = new File("Отчет по практике.docx");
        studyFolder1Curs.add(firstLabDocx);
        studyFolder1Curs.add(practiceReport);

        disk.add(studyFolder1Curs);
        disk.showElements();

        Element studyFolder1Semester = new Folder("1 семестр");
        Element cppBuilder = new File("C++Builder.exe");
        Element firstLab = new File("Отчет по 1 лаборатронй работе.docx");
        studyFolder1Semester.add(cppBuilder);
        studyFolder1Semester.add(firstLab);

        disk.add(studyFolder1Semester);
//        disk.showElements();

        disk.remove(studyFolder1Curs);
    }

}
