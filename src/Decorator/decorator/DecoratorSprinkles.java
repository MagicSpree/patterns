package Decorator.decorator;

import Decorator.product.Product;

public class DecoratorSprinkles implements Product {
    private final Product product;

    public DecoratorSprinkles(Product product) {
        this.product = product;
    }

    @Override
    public String getTitle() {
        return product.getTitle() + " с добавлением присыпки";
    }

    @Override
    public double getPrice() {
        return product.getPrice() + 20;
    }
}
