package Decorator.decorator;

import Decorator.product.Product;

public class DecoratorSyrup implements Product {
    private final Product product;

    public DecoratorSyrup(Product product) {
        this.product = product;
    }

    @Override
    public String getTitle() {
        return product.getTitle() + " с добавлением сиропа";
    }

    @Override
    public double getPrice() {
        return product.getPrice() + 50;
    }
}
