package Decorator;

import Decorator.decorator.DecoratorSprinkles;
import Decorator.decorator.DecoratorSyrup;
import Decorator.product.IceCream;
import Decorator.product.Product;
import Decorator.product.Smoothie;
import Decorator.product.Yogurt;

public class Main {

    public static void main(String[] args) {
        menuProducts(new IceCream());
        menuProducts(new Yogurt());
        menuProducts(new Smoothie());
    }

    static void menuProducts(Product product) {
        Product productWithSyrup = new DecoratorSyrup(product);
        Product productWithSprinkles = new DecoratorSprinkles(product);
        Product productWithSprinklesAndSyrup = new DecoratorSprinkles(new DecoratorSyrup(product));
        print(product);
        print(productWithSyrup);
        print(productWithSprinkles);
        print(productWithSprinklesAndSyrup);
    }

    static void print(Product product)
    {
        System.out.printf("Продукт %s стоит %.1f \n", product.getTitle(), product.getPrice());
    }
}
