package Decorator.product;

public class Yogurt implements Product {
    @Override
    public String getTitle() {
        return "Йогурт";
    }

    @Override
    public double getPrice() {
        return 43;
    }
}
