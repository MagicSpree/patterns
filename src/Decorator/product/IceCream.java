package Decorator.product;

public class IceCream implements Product {

    @Override
    public String getTitle() {
        return "Мороженое";
    }

    @Override
    public double getPrice() {
        return 40;
    }
}
