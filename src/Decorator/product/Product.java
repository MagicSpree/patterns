package Decorator.product;

public interface Product {
    String getTitle();
    double getPrice();
}
