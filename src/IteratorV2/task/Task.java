package IteratorV2.task;

public class Task {

    private final String nameTask;

    public Task(String nameTask) {
        this.nameTask = nameTask;
    }

    public String getNameTask() {
        return nameTask;
    }

    @Override
    public String toString() {
        return nameTask;
    }
}
