package IteratorV2.iterator;

public interface Iterator<E> {
    boolean hasNext();
    E getNext();
    void remove();
}
