package Composite.composite;

import Composite.composite.Element;

import java.util.ArrayList;
import java.util.List;

public class Folder extends AbstractElement {

    List<Element> children = new ArrayList<>();

    public Folder(String nameElement) {
        super(nameElement);
    }

    @Override
    public void showElements() {
        System.out.println("Папка: " + getNameElement());
        System.out.println("Содержимое папки " + getNameElement() + ":");

        for (Element element : children) {
            element.showElements();
        }
    }

    public void add(Element element) {
        children.add(element);
    }

    public void delete(Element element) {
        children.remove(element);
    }

}
