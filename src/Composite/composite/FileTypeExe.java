package Composite.composite;

public class FileTypeExe extends AbstractElement{

    public FileTypeExe(String nameElement) {
        super(nameElement);
    }

    @Override
    public void showElements() {
        System.out.println(getNameElement());
    }

}
