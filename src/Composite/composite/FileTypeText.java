package Composite.composite;

public class FileTypeText extends AbstractElement {

    public FileTypeText(String nameElement) {
        super(nameElement);
    }

    @Override
    public void showElements() {
        System.out.println(getNameElement());
    }

}
