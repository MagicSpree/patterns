package Composite;

import Composite.composite.FileTypeExe;
import Composite.composite.FileTypeText;
import Composite.composite.Folder;

public class Main {
    public static void main(String[] args) {
        Folder disk = new Folder("Диск С");
        Folder studyFolder1Curs = new Folder("Обучение 1 курс");
        FileTypeText firstLabDocx = new FileTypeText("1lab.docx");
        FileTypeText practiceReport = new FileTypeText("Отчет по практике.docx");
        studyFolder1Curs.add(firstLabDocx);
        studyFolder1Curs.add(practiceReport);

        disk.add(studyFolder1Curs);
//        disk.showElements();

        Folder studyFolder1Semester = new Folder("1 семестр");
        FileTypeExe cppBuilder = new FileTypeExe("C++Builder.exe");
        FileTypeText firstLab = new FileTypeText("Отчет по 1 лаборатронй работе.docx");
        studyFolder1Semester.add(cppBuilder);
        studyFolder1Semester.add(firstLab);

        disk.add(studyFolder1Semester);
//        disk.showElements();

        disk.delete(studyFolder1Curs);
        disk.showElements();
    }

}
