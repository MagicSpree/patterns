package Iterator.list;

import Iterator.iterator.Iterator;

import java.util.List;

public interface TaskCollection<E> {
    void addTask(E task);
    void addAllTask(List<E> task);
    void remove(E task);
    Iterator<E> iterator();
}
