package Iterator.list;

import Iterator.iterator.Iterator;
import Iterator.task.Days;
import Iterator.task.Task;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.NoSuchElementException;

public class TaskList implements TaskCollection<Task> {
    private final List<Task> tasks;
    private final Days dayTask;
    protected int modificationCount;

    public TaskList(Days dayTask) {
        tasks = new ArrayList<>();
        this.dayTask = dayTask;
    }

    public TaskList(List<Task> tasks, Days dayTask) {
        this.tasks = tasks;
        this.dayTask = dayTask;
        this.modificationCount = 1;
    }

    @Override
    public void addTask(Task task) {
        this.tasks.add(task);
        modificationCount++;
    }

    @Override
    public void addAllTask(List<Task> task) {
        this.tasks.addAll(task);
        modificationCount++;
    }

    @Override
    public void remove(Task task) {
        this.tasks.remove(task);
        modificationCount++;
    }

    @Override
    public Iterator<Task> iterator() {
        return new IteratorImpl();
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        message.append("День недели: ")
                .append(dayTask.getNameDay())
                .append("\nЗадачи:\n");

        Iterator<Task> iterator = iterator();

        while (iterator.hasNext()) {
            message.append(iterator.getNext())
                    .append("\n");
        }
        return message.toString();
    }


    public class IteratorImpl implements Iterator<Task> {
        private int position;
        int lastElem = -1;
        int expectedModCount = modificationCount;

        @Override
        public boolean hasNext() {
            return position != tasks.size();
        }

        @Override
        public Task getNext() {
            checkModification();
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            if (position >= tasks.size())
                throw new ConcurrentModificationException();
            lastElem = position;
            position++;
            return tasks.get(lastElem);
        }

        @Override
        public void remove() {
            if (lastElem < 0)
                throw new IllegalStateException();
            checkModification();
            tasks.remove(lastElem);
            position = lastElem;
            lastElem = -1;
            expectedModCount = modificationCount;
        }

        private void checkModification() {
            if (modificationCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }
}
