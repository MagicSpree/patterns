package Iterator.iterator;

public interface Iterator<E> {
    boolean hasNext();
    E getNext();
    void remove();
}
