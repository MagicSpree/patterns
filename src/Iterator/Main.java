package Iterator;

import Iterator.list.TaskCollection;
import Iterator.list.TaskList;
import Iterator.task.Days;
import Iterator.task.Task;

import java.util.ArrayList;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        TaskCollection<Task> taskCollectionMonday = new TaskList(Days.Monday);
        taskCollectionMonday.addAllTask(List.of(
                new Task("Сделать зарядку"),
                new Task("Помыться"),
                new Task("Позавтракать"),
                new Task("Почиллить"),
                new Task("Порофлить")
        ));
        System.out.println(taskCollectionMonday);
    }
}
