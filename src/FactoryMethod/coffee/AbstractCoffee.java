package FactoryMethod.coffee;

public abstract class AbstractCoffee implements Coffee {
    @Override
    public void grindingCoffee() {
        print("Перемалываем зерна");
    }

    @Override
    public void pourIntoACup() {
        print("Наливаем кофе в чашку");
    }

    @Override
    public void servingCoffee() {
        print("Подаем чашку с кофе клиенту");
    }

    public void print(String message) {
        System.out.println(message);
    }
}
