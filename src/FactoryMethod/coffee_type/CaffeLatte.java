package FactoryMethod.coffee_type;

import FactoryMethod.coffee.AbstractCoffee;

public class CaffeLatte extends AbstractCoffee {
    @Override
    public void makeCoffee() {
        print("Готовим Латте");
    }
}
