package FactoryMethod.coffee_type;

import FactoryMethod.coffee.AbstractCoffee;

public class Espresso extends AbstractCoffee {
    @Override
    public void makeCoffee() {
        print("Готовим Эспрессо");
    }
}
