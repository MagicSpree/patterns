package FactoryMethod.coffee_type;

import FactoryMethod.coffee.AbstractCoffee;

public class Americano extends AbstractCoffee {

    @Override
    public void makeCoffee() {
        print("Готовим Американо");
    }
}
