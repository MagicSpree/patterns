package FactoryMethod.coffee_type;

import FactoryMethod.coffee.AbstractCoffee;

public class Cappuccino extends AbstractCoffee {
    @Override
    public void makeCoffee() {
        print("Готовим Капучино");
    }
}
