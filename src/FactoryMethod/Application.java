package FactoryMethod;

import FactoryMethod.factory.CoffeeShopFactory;
import FactoryMethod.factory.types_factory.AmericanoFactory;
import FactoryMethod.factory.types_factory.CaffeLatteFactory;
import FactoryMethod.factory.types_factory.CappuccinoFactory;
import FactoryMethod.factory.types_factory.EspressoFactory;

public class Application {
    public static void main(String[] args) {
        orderCoffee(new AmericanoFactory());
        orderCoffee(new CaffeLatteFactory());
        orderCoffee(new CappuccinoFactory());
        orderCoffee(new EspressoFactory());
    }

    private static void orderCoffee(CoffeeShopFactory coffeeShop){
        coffeeShop.orderCoffee();
    }
}
