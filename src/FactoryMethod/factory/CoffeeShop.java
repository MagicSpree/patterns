package FactoryMethod.factory;

import FactoryMethod.coffee.Coffee;

public abstract class CoffeeShop implements CoffeeShopFactory {

    @Override
    public void orderCoffee() {
        Coffee coffee = createCoffee();
        coffee.grindingCoffee();
        coffee.makeCoffee();
        coffee.pourIntoACup();
        coffee.servingCoffee();
    }
}
