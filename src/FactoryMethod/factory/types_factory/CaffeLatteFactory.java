package FactoryMethod.factory.types_factory;

import FactoryMethod.coffee.Coffee;
import FactoryMethod.coffee_type.CaffeLatte;
import FactoryMethod.factory.CoffeeShop;

public class CaffeLatteFactory extends CoffeeShop {
    @Override
    public Coffee createCoffee() {
        return new CaffeLatte();
    }
}
