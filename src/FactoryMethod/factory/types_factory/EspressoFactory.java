package FactoryMethod.factory.types_factory;

import FactoryMethod.coffee.Coffee;
import FactoryMethod.coffee_type.Espresso;
import FactoryMethod.factory.CoffeeShop;

public class EspressoFactory extends CoffeeShop {
    @Override
    public Coffee createCoffee() {
        return new Espresso();
    }
}
