package FactoryMethod.factory.types_factory;

import FactoryMethod.coffee.Coffee;
import FactoryMethod.coffee_type.Cappuccino;
import FactoryMethod.factory.CoffeeShop;

public class CappuccinoFactory extends CoffeeShop {
    @Override
    public Coffee createCoffee() {
        return new Cappuccino();
    }
}
