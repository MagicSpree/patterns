package FactoryMethod.factory.types_factory;

import FactoryMethod.coffee.Coffee;
import FactoryMethod.coffee_type.Americano;
import FactoryMethod.factory.CoffeeShop;

public class AmericanoFactory extends CoffeeShop {
    @Override
    public Coffee createCoffee() {
        return new Americano();
    }
}
