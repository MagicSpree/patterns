package FactoryMethod.factory;

import FactoryMethod.coffee.Coffee;

public interface CoffeeShopFactory {
    void orderCoffee();
    Coffee createCoffee();
}
