package Builder;

import Builder.builder.BuilderProduct;
import Builder.builder.GlassBuilderProduct;
import Builder.director.Director;
import Builder.product.Glass;
import Builder.product.Product;

public class Main {
    public static void main(String[] args) {
        BuilderProduct builderProduct = new GlassBuilderProduct();
        Director director = new Director();
        director.constructPerfectGlass(builderProduct);
        Product perfectGlass = builderProduct.getResult();
        System.out.println(perfectGlass);
    }
}
