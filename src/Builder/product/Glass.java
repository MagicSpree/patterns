package Builder.product;

import Builder.manufacturer.Manufacturer;
import Builder.material.Material;

public class Glass implements Product{
    private final Manufacturer manufacturer;
    private final Material material;
    private final double price;

    public Glass(Manufacturer manufacturer, Material material, double price) {
        this.manufacturer = manufacturer;
        this.material = material;
        this.price = price;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public Material getMaterial() {
        return material;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Бокал {" +
                "Производитель= " + manufacturer.getName() +
                ", Материал= " + material.getNameMaterial() +
                ", Цена= " + price +
                '}';
    }
}
