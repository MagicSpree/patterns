package Builder.material;

public enum MaterialType implements Material{
    CRYSTAL("Хрусталь"),
    GLASS("Стекло"),
    PLASTIC("Пластик");

    private String nameMaterial;

    MaterialType(String nameMaterial) {
        this.nameMaterial = nameMaterial;
    }

    @Override
    public String getNameMaterial() {
        return nameMaterial;
    }
}
