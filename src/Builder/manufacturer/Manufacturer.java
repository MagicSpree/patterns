package Builder.manufacturer;

public interface Manufacturer {
    String getName();
}
