package Builder.manufacturer;

public class ManufacturerGlass implements Manufacturer {

    private final String nameManufacturer;

    public ManufacturerGlass(String nameManufacturer) {
        this.nameManufacturer = nameManufacturer;
    }

    @Override
    public String getName() {
        return nameManufacturer;
    }
}
