package Builder.director;

import Builder.builder.BuilderProduct;
import Builder.manufacturer.ManufacturerGlass;
import Builder.material.MaterialType;

public class Director {

    public void constructCheapGlass(BuilderProduct builderProduct) {
        builderProduct.setManufacturer(new ManufacturerGlass("Никольский стекольный завод"));
        builderProduct.setMaterial(MaterialType.PLASTIC);
        builderProduct.setPrice(50);
    }

    public void constructNormalGlass(BuilderProduct builderProduct) {
        builderProduct.setManufacturer(new ManufacturerGlass("Красное Эхо"));
        builderProduct.setMaterial(MaterialType.GLASS);
        builderProduct.setPrice(75);
    }

    public void constructPerfectGlass(BuilderProduct builderProduct) {
        builderProduct.setManufacturer(new ManufacturerGlass("Дятьковский хрусталь"));
        builderProduct.setMaterial(MaterialType.CRYSTAL);
        builderProduct.setPrice(100);
    }
}
