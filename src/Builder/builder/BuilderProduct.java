package Builder.builder;

import Builder.product.Glass;
import Builder.manufacturer.Manufacturer;
import Builder.material.Material;
import Builder.product.Product;

public interface BuilderProduct {
    void setManufacturer(Manufacturer manufacturer);
    void setMaterial(Material material);
    void setPrice(double price);
    Product getResult();
}
