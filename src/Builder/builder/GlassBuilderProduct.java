package Builder.builder;

import Builder.product.Glass;
import Builder.manufacturer.Manufacturer;
import Builder.material.Material;

public class GlassBuilderProduct implements BuilderProduct {

    private Manufacturer manufacturer;
    private Material material;
    private double price;

    @Override
    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public Glass getResult() {
        return new Glass(manufacturer, material, price);
    }
}
