package Strategy.main_strategy;

public interface PayStrategy {
    void pay(double paymentAmount);
}
