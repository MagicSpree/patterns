package Strategy.type_pay;

import State.utils.Utils;
import Strategy.main_strategy.PayStrategy;

public class PayByCash implements PayStrategy {

    @Override
    public void pay(double paymentAmount) {
        Utils.print("Оплата в размере " + paymentAmount + " с помощью наличных средств");;
    }
}
