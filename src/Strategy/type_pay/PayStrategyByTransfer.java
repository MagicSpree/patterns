package Strategy.type_pay;

import State.utils.Utils;
import Strategy.entity.CreditCart;
import Strategy.main_strategy.PayStrategy;

public class PayStrategyByTransfer implements PayStrategy {
    private final CreditCart targetCart;

    public PayStrategyByTransfer(CreditCart targetCart) {
        this.targetCart = targetCart;
    }

    private boolean checkValidBalance(double paymentAmount) {
        return paymentAmount > targetCart.getAmount();
    }

    @Override
    public void pay(double paymentAmount) {
        String result = "Ошибка. Недостаточной средств на карте";
        if (!targetCart.checkValidBalance(paymentAmount))
        {
            result = "Осуществлен перевод в размере " + paymentAmount + " на другую карту";
            targetCart.transferAmount(paymentAmount);
        }
        Utils.print(result);
    }
}
