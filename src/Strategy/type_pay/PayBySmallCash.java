package Strategy.type_pay;

import State.utils.Utils;
import Strategy.main_strategy.PayStrategy;

public class PayBySmallCash implements PayStrategy {
    @Override
    public void pay(double paymentAmount) {
        Utils.print("Оплата в размере " + paymentAmount + " с помощью мелочи");;
    }
}
