package Strategy.type_pay;

import State.utils.Utils;
import Strategy.entity.CreditCart;
import Strategy.main_strategy.PayStrategy;

public class PayByCreditCard implements PayStrategy {

    private final CreditCart creditCart;

    public PayByCreditCard(CreditCart creditCart) {
        this.creditCart = creditCart;
    }


    @Override
    public void pay(double paymentAmount) {
        String result = "Оплата в размере " + paymentAmount + " с помощью кредитной карты";
        if (creditCart.checkValidBalance(paymentAmount))
        {
            result = "Ошибка. Недостаточной средств на карте";
        }
        Utils.print(result);
    }
}
