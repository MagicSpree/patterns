package Strategy.entity;

public class CreditCart {
    private int amount;
    private int number;
    private String date;
    private int cvv;

    public CreditCart(int number, String date, int cvv) {
        this.amount = 5000;
        this.number = number;
        this.date = date;
        this.cvv = cvv;
    }

    public int getAmount() {
        return amount;
    }

    public void transferAmount(double amount) {
        this.amount += amount;
    }

    public boolean checkValidBalance(double paymentAmount) {
        return paymentAmount > getAmount();
    }
}
