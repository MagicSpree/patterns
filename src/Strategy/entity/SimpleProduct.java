package Strategy.entity;

public class SimpleProduct implements Product {

    private final String nameProduct;
    private final double amount;

    public SimpleProduct(String nameProduct, double amount) {
        this.nameProduct = nameProduct;
        this.amount = amount;
    }

    @Override
    public String getNameProduct() {
        return nameProduct;
    }

    @Override
    public double getPrice() {
        return amount;
    }
}
