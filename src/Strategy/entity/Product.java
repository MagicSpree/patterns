package Strategy.entity;

public interface Product {
    String getNameProduct();
    double getPrice();
}
