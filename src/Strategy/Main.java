package Strategy;

import Strategy.entity.CreditCart;
import Strategy.entity.Product;
import Strategy.entity.SimpleProduct;
import Strategy.main_strategy.PayStrategy;
import Strategy.type_pay.PayBySmallCash;
import Strategy.type_pay.PayStrategyByTransfer;
import Strategy.type_pay.PayByCash;
import Strategy.type_pay.PayByCreditCard;

public class Main {
    public static void main(String[] args) {
        Product product = new SimpleProduct("Лапша", 1000);
        CreditCart creditCart = new CreditCart(123, "222", 44);

        order(new PayByCash(), product);
        order(new PayBySmallCash(), product);
        order(new PayByCreditCard(creditCart), product);
        order(new PayStrategyByTransfer(creditCart), product);
    }

    private static void order(PayStrategy payStrategy, Product product) {
        payStrategy.pay(product.getPrice());
    }
}
