package Templace.entity;

import Templace.utils.Utils;

public class GermanyFlag extends Flag{

    public GermanyFlag(String nameFlag) {
        super(nameFlag);
    }

    @Override
    void paintUpperBand() {
        Utils.print("Чёрная полоса");
    }

    @Override
    void paintMiddleBand() {
        Utils.print("Красная полоса");
    }

    @Override
    void paintLowerBand() {
        Utils.print("Желтая полоса");
    }
}
