package Templace.entity;

import Templace.utils.Utils;

public class RussianFlag extends Flag{

    public RussianFlag(String nameFlag) {
        super(nameFlag);
    }

    @Override
    void paintUpperBand() {
        Utils.print("Белая полоса");
    }

    @Override
    void paintMiddleBand() {
        Utils.print("Синяя полоса");
    }

    @Override
    void paintLowerBand() {
        Utils.print("Красная полоса");
    }
}
