package Templace.entity;

import Templace.templat.Template;

public abstract class Flag implements Template {

    private final String nameFlag;

    public Flag(String nameFlag) {
        this.nameFlag = nameFlag;
    }

    @Override
    public void paintFlag() {
        paintUpperBand();
        paintMiddleBand();
        paintLowerBand();
    }

    @Override
    public String getNameFlag() {
        return nameFlag;
    }

    abstract void paintUpperBand();

    abstract void paintMiddleBand();

    abstract void paintLowerBand();
}
