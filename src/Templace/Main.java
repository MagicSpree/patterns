package Templace;

import Templace.entity.GermanyFlag;
import Templace.entity.RussianFlag;
import Templace.templat.Template;
import Templace.utils.Utils;

public class Main {
    public static void main(String[] args) {
        Template russianFlag = new RussianFlag("Флаг России");
        Template germanyFlag = new GermanyFlag("Флаг Германии");
        printColorFlag(russianFlag);
        printColorFlag(germanyFlag);
    }

    private static void printColorFlag(Template template) {
        Utils.print(template.getNameFlag());
        template.paintFlag();
    }
}
