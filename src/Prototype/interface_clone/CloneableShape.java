package Prototype.interface_clone;

public interface CloneableShape {
    CloneableShape copy();
    String getInfo();
}
