package Prototype;

import Prototype.interface_clone.CloneableShape;
import Prototype.shapes.Rectangle;
import Prototype.shapes.Square;
//import Prototype.shapes.Square;

public class Main {
    public static void main(String[] args) {
        CloneableShape rectangle = new Rectangle(10, 5);
        CloneableShape square = new Square(10);

        System.out.println(rectangle.getInfo());
        System.out.println(square.getInfo());

        CloneableShape rectangleClone = rectangle.copy();
        CloneableShape squareClone = square.copy();

        cloneObject(rectangleClone);
        cloneObject(squareClone);
    }

    public static void cloneObject(CloneableShape cloneableShape) {
        System.out.println(cloneableShape.getInfo());
    }
}
