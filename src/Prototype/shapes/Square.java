package Prototype.shapes;

import Prototype.interface_clone.CloneableShape;

public class Square extends Rectangle implements CloneableShape{

    public Square(int width) {
       super(width, width);
    }

    @Override
    public CloneableShape clone() {
        return new Square(width);
    }

    @Override
    public String getInfo() {
        return String.format("Квадрат стороной %d", width);
    }
}
