package Prototype.shapes;

import Prototype.interface_clone.CloneableShape;

public class Rectangle implements CloneableShape {

    protected int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public CloneableShape copy() {
        return new Rectangle(this.width, this.height);
    }

    @Override
    public String getInfo() {
        return String.format("Прямоугольник длиной %d и шириной %d", height, width);
    }
}
