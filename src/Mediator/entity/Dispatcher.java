package Mediator.entity;

import Mediator.main_mediator.Mediator;

public class Dispatcher implements AirportObject{

    private final Mediator mediator;
    private final String nameDispatcher;

    public Dispatcher(Mediator mediator, String nameDispatcher) {
        this.mediator = mediator;
        this.nameDispatcher = nameDispatcher;
    }

    @Override
    public String getName() {
        return nameDispatcher;
    }

    @Override
    public void sendMessage(String message) {
        mediator.notify(message, this);
    }

    @Override
    public void getMessage(String message) {
        System.out.println(this.nameDispatcher + " получил информацию от самолета: " + message);
    }
}
