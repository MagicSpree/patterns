package Mediator.entity;

import Mediator.entity.AirportObject;
import Mediator.main_mediator.Mediator;

public class SimpleAirportObject implements AirportObject {

    private final Mediator mediator;
    private final String nameAirplane;

    public SimpleAirportObject(Mediator mediator, String nameAirplane) {
        this.mediator = mediator;
        this.nameAirplane = nameAirplane;
    }

    @Override
    public String getName() {
        return nameAirplane;
    }

    @Override
    public void sendMessage(String message) {
        mediator.notify(message, this);
    }

    @Override
    public void getMessage(String message) {
        System.out.println(getName() + " поступила информация: " + message);
    }
}
