package Mediator.entity;

public interface AirportObject {
    String getName();
    void sendMessage(String message);
    void getMessage(String message);
}
