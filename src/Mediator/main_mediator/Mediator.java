package Mediator.main_mediator;

import Mediator.entity.AirportObject;

public interface Mediator {
    void notify(String message, AirportObject airportObject);
    void addAirplane(AirportObject airportObject);
    void setDispatcher(AirportObject dispatcher);
}
