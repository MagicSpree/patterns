package Mediator.main_mediator;

import Mediator.entity.AirportObject;
import Mediator.entity.Dispatcher;

import java.util.ArrayList;
import java.util.List;

public class Chat implements Mediator {
    AirportObject dispatcher;
    List<AirportObject> boeings = new ArrayList<>();

    @Override
    public void notify(String message, AirportObject airportObject) {
        if (dispatcher != airportObject)
            dispatcher.getMessage(message);

        for (AirportObject b: boeings) {
            if (b != airportObject) {
                b.getMessage(message);
            }
        }
    }

    @Override
    public void addAirplane(AirportObject airportObject) {
        boeings.add(airportObject);
    }

    @Override
    public void setDispatcher(AirportObject dispatcher) {
        this.dispatcher = dispatcher;
    }
}
