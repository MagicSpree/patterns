package Mediator;

import Mediator.entity.AirportObject;
import Mediator.entity.Dispatcher;
import Mediator.entity.SimpleAirportObject;
import Mediator.main_mediator.Chat;
import Mediator.main_mediator.Mediator;

public class Main {
    public static void main(String[] args) {
        Mediator mediator = new Chat();

        AirportObject dispatcher = new Dispatcher(mediator, "Соловьев");

        AirportObject boeing7731 = new SimpleAirportObject(mediator, "Boeing-7731");
        AirportObject superJet = new SimpleAirportObject(mediator, "SuperJet");
        AirportObject macish = new SimpleAirportObject(mediator, "Macish");

        mediator.setDispatcher(dispatcher);
        mediator.addAirplane(boeing7731);
        mediator.addAirplane(superJet);
        mediator.addAirplane(macish);

        boeing7731.sendMessage("Иду на посадку!!");
        macish.sendMessage("Взлетаю!!!");
        dispatcher.sendMessage("Всем приготоввиться!!!");
    }
}
