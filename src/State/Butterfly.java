package State;

import State.main_state.State;
import State.type_state.Caterpillar;

public class Butterfly {
    private State state;

    public Butterfly() {
        this.state = new Caterpillar(this);
    }

    public void evolution() {
        state.evolution();
    }

    public void doIt() {
        state.doIt();
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
