package State.main_state;

public interface State {
    void evolution();
    String doIt();
    String descriptionState();
}
