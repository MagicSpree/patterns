package State.main_state;

import State.Butterfly;
import State.utils.Utils;

public abstract class StateImpl implements State {

    protected Butterfly butterfly;

    public StateImpl(Butterfly butterfly) {
        this.butterfly = butterfly;
        getAllInfoState();
    }

    public void getAllInfoState() {
        Utils.print(descriptionState() +" - "+ doIt());
    }
}
