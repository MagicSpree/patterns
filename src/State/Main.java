package State;

public class Main {

    public static void main(String[] args) {
        Butterfly butterfly = new Butterfly();
        butterfly.evolution();
    }

//    static void evolutionButterfly(State butterflyState) {
//        butterflyState.evolution();
//    }
}
