package State.type_state;

import State.main_state.State;
import State.main_state.StateImpl;
import State.utils.Utils;
import State.Butterfly;

public class Cocoon extends StateImpl implements State {

    public Cocoon(Butterfly butterfly) {
        super(butterfly);
    }

    @Override
    public void evolution() {
        butterfly.setState(new FullButterfly(butterfly));
        butterfly.evolution();
    }

    @Override
    public String doIt() {
        return "Она может спать";
    }

    @Override
    public String descriptionState() {
        return "Бабочка окукливается";
    }
}
