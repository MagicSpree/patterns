package State.type_state;

import State.main_state.State;
import State.main_state.StateImpl;
import State.utils.Utils;
import State.Butterfly;

public class FullButterfly extends StateImpl implements State {

    public FullButterfly(Butterfly butterfly) {
        super(butterfly);
    }

    @Override
    public void evolution() {
        Utils.print("Эволюция на этом закончилась:(");
    }

    @Override
    public String doIt() {
        return "Она может летать";
    }

    @Override
    public String descriptionState() {
        return "Становится бабочкой";
    }
}
