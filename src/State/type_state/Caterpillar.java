package State.type_state;

import State.Butterfly;
import State.main_state.State;
import State.main_state.StateImpl;

public class Caterpillar extends StateImpl implements State {

    public Caterpillar(Butterfly butterfly) {
        super(butterfly);
    }

    @Override
    public void evolution() {
        butterfly.setState(new Cocoon(butterfly));
        butterfly.evolution();
    }

    @Override
    public String doIt() {
        return "Она может ползать";
    }

    @Override
    public String descriptionState() {
        return "Бабочка ещё гусеница";
    }
}
