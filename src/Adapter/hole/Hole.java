package Adapter.hole;

import Adapter.peg.RoundPeg;

public abstract class Hole {
    protected double radius;

    public Hole(double radius) {
        this.radius = radius;
    }

    public abstract boolean isFit(RoundPeg peg);
}
