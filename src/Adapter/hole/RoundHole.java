package Adapter.hole;

import Adapter.peg.RoundPeg;

public class RoundHole extends Hole{

    public RoundHole(double radius) {
        super(radius);
    }

    @Override
    public boolean isFit(RoundPeg peg) {
        return radius >= peg.getDiameter();
    }
}
