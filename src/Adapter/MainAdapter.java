package Adapter;

import Adapter.adapter.SquarePegAdapter;
import Adapter.hole.RoundHole;
import Adapter.peg.RoundPeg;
import Adapter.peg.SquarePeg;

public class MainAdapter {

    public static void main(String[] args) {
        RoundHole roundHole = new RoundHole(5);

        RoundPeg roundPeg = new RoundPeg(11);
        String isPlacedRoundPeg = roundHole.isFit(roundPeg) ? "помещается" : "не помещается";
        print("Круглое колышко " + isPlacedRoundPeg + " в круглое отверстие");

        SquarePeg squarePeg = new SquarePeg(2);
        SquarePegAdapter squarePegAdapter = new SquarePegAdapter(squarePeg);
        String isPlaced = roundHole.isFit(squarePegAdapter) ? "помещается" : "не помещается";
        print("Квадратное колышко " + isPlaced + " в круглое отверстие");
    }

    private static void print(String msg) {
        System.out.println(msg);
    }
}
