package Facade;

import Facade.facade.PreparationFacade;

public class Main {
    public static void main(String[] args) {
        PreparationFacade preparationFacade = new PreparationFacade();
        int countOfPancakeCooked = preparationFacade.makePancakes(8).size();
        System.out.println("Было испечено " + countOfPancakeCooked + " блинов");
    }
}
