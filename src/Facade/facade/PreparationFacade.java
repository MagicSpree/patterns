package Facade.facade;

import Facade.pancake.Dough;
import Facade.pancake.Pancake;
import Facade.pancake.Tableware;

import java.util.ArrayList;
import java.util.List;

public class PreparationFacade {

    public List<Pancake> makePancakes(int countPancake) {
        if (countPancake == 0 || countPancake < 0) {
            throw new IllegalArgumentException("Количество блинов не может быть меньше или равно 0");
        }
        new Tableware();
        new Dough();

        List<Pancake> pancakeList = new ArrayList<>(countPancake);

        for (int i = 0; i < countPancake; i++) {
            pancakeList.add(new Pancake());
        }
        return pancakeList;
    }
}
