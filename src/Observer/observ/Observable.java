package Observer.observ;

public interface Observable<T> {
    void addObserver(T listener);
    void removeObserver(T listener);
    void removeAll();
    void notifyObserver(Object object);
}
