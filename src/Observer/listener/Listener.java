package Observer.listener;

public interface Listener {
    void update(Object object);
}
