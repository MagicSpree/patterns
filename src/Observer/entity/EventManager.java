package Observer.entity;

import Observer.listener.Listener;
import Observer.observ.Observable;

import java.util.ArrayList;
import java.util.List;

public class EventManager implements Observable<Listener> {

    private final List<Listener> observer = new ArrayList<>();

    @Override
    public void addObserver(Listener listener) {
        observer.add(listener);
    }

    @Override
    public void removeObserver(Listener listener) {
        observer.remove(listener);
    }

    @Override
    public void removeAll() {
        observer.clear();
    }

    @Override
    public void notifyObserver(Object object) {
        for (Listener l: observer) {
            l.update(object);
        }
    }
}
