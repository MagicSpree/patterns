package Observer.entity;


public class Publisher {

    public EventManager events;

    public Publisher() {
        this.events = new EventManager();
    }

    public void beginSalesNewBook(){
        events.notifyObserver("Произошла закупка новых книг Харуки Мураками");
    }

}
