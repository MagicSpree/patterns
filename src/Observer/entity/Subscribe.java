package Observer.entity;

import Observer.listener.Listener;

public class Subscribe implements Listener {

    String nameSubscribe;

    public Subscribe(String nameSubscribe) {
        this.nameSubscribe = nameSubscribe;
    }

    @Override
    public void update(Object object) {
        System.out.printf("Пользователь: %s получил уведомление от Издателя: %s \n", nameSubscribe, object);
    }
}
