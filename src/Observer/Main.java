package Observer;

import Observer.entity.Publisher;
import Observer.entity.Subscribe;

public class Main {

    public static void main(String[] args) {
        Publisher publisher = new Publisher();
        publisher.events.addObserver(new Subscribe("Черненко"));
        publisher.events.addObserver(new Subscribe("Воронков"));
        publisher.events.addObserver(new Subscribe("Светилов"));
        publisher.beginSalesNewBook();
    }
}
