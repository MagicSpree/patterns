package Factory.coffee_type;

import Factory.coffee.AbstractCoffee;

public class Americano extends AbstractCoffee {

    @Override
    public void makeCoffee() {
        print("Готовим Американо");
    }
}
