package Factory.coffee_type;

import Factory.coffee.AbstractCoffee;

public class Cappuccino extends AbstractCoffee {
    @Override
    public void makeCoffee() {
        print("Готовим Капучино");
    }
}
