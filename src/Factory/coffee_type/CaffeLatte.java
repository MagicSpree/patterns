package Factory.coffee_type;

import Factory.coffee.AbstractCoffee;

public class CaffeLatte extends AbstractCoffee {
    @Override
    public void makeCoffee() {
        print("Готовим Латте");
    }
}
