package Factory.coffee_type;

import Factory.coffee.AbstractCoffee;

public class Espresso extends AbstractCoffee {
    @Override
    public void makeCoffee() {
        print("Готовим Эспрессо");
    }
}
