package Factory;

import Factory.coffee_enum.TypesCoffee;
import Factory.coffee_factory.CoffeeFactory;

public class Main {
    public static void main(String[] args) {
        CoffeeShop coffeeShop = new CoffeeShop(new CoffeeFactory());
        coffeeShop.orderCoffee(TypesCoffee.AMERICANO);
    }
}
