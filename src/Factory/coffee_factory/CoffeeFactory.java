package Factory.coffee_factory;

import Factory.coffee.Coffee;
import Factory.coffee_enum.TypesCoffee;
import Factory.coffee_type.Americano;
import Factory.coffee_type.CaffeLatte;
import Factory.coffee_type.Cappuccino;
import Factory.coffee_type.Espresso;

public class CoffeeFactory implements Factory{

    @Override
    public Coffee createCoffee(TypesCoffee typesCoffee) {
        Coffee coffee = null;
        switch (typesCoffee) {
            case AMERICANO:
                coffee = new Americano();
                break;
            case ESPRESSO:
                coffee = new Espresso();
                break;
            case CAPPUCCINO:
                coffee = new Cappuccino();
                break;
            case CAFFE_LATTE:
                coffee = new CaffeLatte();
                break;
        }
        return coffee;
    }
}
