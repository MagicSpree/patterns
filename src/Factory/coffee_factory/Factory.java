package Factory.coffee_factory;

import Factory.coffee.Coffee;
import Factory.coffee_enum.TypesCoffee;

public interface Factory {
    Coffee createCoffee(TypesCoffee typesCoffee);
}
