package Factory.coffee_enum;

public enum TypesCoffee {
    ESPRESSO,
    AMERICANO,
    CAFFE_LATTE,
    CAPPUCCINO
}
