package Factory.coffee;

public interface Coffee {
    void grindingCoffee();
    void makeCoffee();
    void pourIntoACup();
    void servingCoffee();
}
