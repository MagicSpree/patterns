package Factory;

import Factory.coffee.Coffee;
import Factory.coffee_enum.TypesCoffee;
import Factory.coffee_factory.CoffeeFactory;
import Factory.coffee_factory.Factory;

public class CoffeeShop {

    private final Factory coffeeFactory;

    public CoffeeShop(Factory coffeeFactory) {
        this.coffeeFactory = coffeeFactory;
    }

    public void orderCoffee(TypesCoffee typesCoffee) {
        Coffee coffee = coffeeFactory.createCoffee(typesCoffee);
        coffee.grindingCoffee();
        coffee.makeCoffee();
        coffee.pourIntoACup();
        coffee.servingCoffee();
    }
}
